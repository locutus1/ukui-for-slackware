#!/bin/bash 

for i in \
libuchardet \
geoclue2 \
libgusb \
colord \
lsb-release \
imlib2 \
; do
sbopkg -B -i ${i} || exit 1
done
