# UKUI For Slackware 15

This project based on [UKUI](https://www.ukui.org/) &  [Slackware 15 / Current ](https://docs.slackware.com/slackware:current) 

## Getting started



Slackware 15 / current basic install 

http://slackware.uk/people/alien-current-iso/

You just have to install Slackware current with the standard selected packages.

## Preparation

groupadd -g 303 colord<br>
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord


## Dependencies

UKUI is a Desktop Environment initially developed to work on Ubuntu Kylin, but actually UKUI is a fork of the Mate desktop environment.
You can probably tell what we need to do next,obviously we have to install the Mate desktop environment.

Make it easy to build and install by using standard style [SlackBuild](https://gitlab.com/mateslackbuilds/msb), but I recommend to install Mate (msb) via [slackpkg+](https://slakfinder.org/slackpkg+15/pkg/).

There are other dependencies but all of them are available on Ponce [slackbuild](https://github.com/Ponce/slackbuilds) repository, alternatively you can install dependencies with 
the build-deps.sh script. The build-deps.sh script based on [sbopkg](https://sbopkg.org/test/sbopkg-0.38.2-noarch-1_wsr.tgz).

`libuchardet geoclue2 libgusb colord lsb-release imlib2`



## Building Instructions

**Important**

_My scripts need root privileges, no sudo._

```
$ su -
# git clone https://gitlab.com/slackernetuk/ukui-for-slackware.git
# cd ukui-for-slackware
# ./build.sh
```

**Configuration**



Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`


`xwmconfig` to setup UKUI as standard-session


**ScreenShot**


![ukui](/uploads/57c856835ee8e6f55912691d7be26593/ukui.png)

